#include <iostream>
#include <stdint.h>
#include <stdio.h>
using namespace std;


// Cette partie du code permet d'initialiser les calculs, on va indiquer a l'ordinateur comment effectuer les calculs que l'utilisateur aura choisi de faire.

/// Addition avec des variables en entier 32 bit
int32_t addition( int32_t A , int32_t B)
{
	int32_t Resultat;

	Resultat = A + B;
	return Resultat;
}
/// Soustraction avec des variables en entier 32 bit
int32_t soustraction( int32_t A , int32_t B)
{
	int32_t Resultat;

	Resultat = A - B;
	return Resultat;
}
/// Multiplication avec des variables en entier 32 bit
int32_t multiplication( int32_t A , int32_t B)
{
	int32_t Resultat;

	Resultat = A * B;
	return Resultat;
}
/// Division avec des variables en entier 32 bit
int32_t division( int32_t A , int32_t B)
{
	int32_t Resultat;

	Resultat = A / B;
	return Resultat;
}
/// Modulo avec des variables en entier 32 bit
int32_t modulo( int32_t A , int32_t B)
{
	int32_t Resultat;

	Resultat = A % B;
	return Resultat;
}


using namespace std;
 
int main()
{	/// déclaration des variables
		int a, b, resultat;
		int choix;
 

	/// Choix du type de calculs
    cout <<"Veuiller saisir le chiffre qui correspond a l'operation souhaitee\n"<< endl;
 
    cout <<"1 Addition"<< endl;
    cout <<"2 Soustraction"<< endl;
    cout <<"3 Multiplication"<< endl;
    cout <<"4 Division"<< endl;
	cout <<"5 Modulo"<< endl; 
    cin >>choix;

	/// Le choix saisie par l'utilisateur est récupérer et renvoi au calcul correspondant
     switch (choix)
   {

	/// Addition
    case 1:
    printf("Entrer le premier nombre : ");
	scanf("%d", &a);
    printf("Entrer le second nombre : ");
	scanf("%d", &b);

	resultat = add( a , b);
	cout<<"le resultat de l'addition "<<a<<" + "<<b<<" = "<<resultat<<endl;
    break;

	/// Soustraction
    case 2:
    printf("Entrer le premier nombre : ");
	scanf("%d", &a);
    printf("Entrer le second nombre : ");
	scanf("%d", &b);

	resultat = sub( a , b);
	cout<<"le resultat de la soustraction "<<a<<" - "<<b<<" = "<<resultat<<endl;
    break;

	/// multiplication
    case 3: 
       printf("Entrer le premier nombre : ");
	scanf("%d", &a);
    printf("Entrer le second nombre : ");
	scanf("%d", &b);

	resultat = mul( a , b);
	cout<<"le resultat de la multiplication "<<a<<" * "<<b<<" = "<<resultat<<endl;
    break;
	/// Division

    case 4:
      printf("Entrer le premier nombre : ");
	scanf("%d", &a);
    printf("Entrer le second nombre : ");
	scanf("%d", &b);

	resultat = divi( a , b);
	cout<<"le resultat de la division "<<a<<" / "<<b<<" = "<<resultat<<endl;
    break;

	/// Modulo
	case 5:
    printf("Entrer le premier nombre : ");
	scanf("%d", &a);
    printf("Entrer le second nombre : ");
	scanf("%d", &b);

	resultat = mod( a , b);
	cout<<"le resultat du modulo "<<a<<" % "<<b<<" = "<<resultat<<endl;
	break;

	default :
		cout<<"vous n'avez pas saisie un chiffre correspondant a une opération"<<endl;
	 }
	
	

    return 0;
}


